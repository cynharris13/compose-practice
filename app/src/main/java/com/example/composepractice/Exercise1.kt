package com.example.composepractice

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.composepractice.ui.theme.ComposePracticeTheme

@Composable
fun BirthdayGreetingWithText(message: String, from: String) {
    Column{
        Text(
            text = message,
            fontSize = 32.sp,
        )
        Text(
            text = from,
            fontSize = 24.sp,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun BirthdayCardPreview() {
    ComposePracticeTheme {
        // A surface container using the 'background' color from the theme
        BirthdayGreetingWithText(message = "Happy Birthday Cynthia!", from = "-From Cynthia")
    }

}