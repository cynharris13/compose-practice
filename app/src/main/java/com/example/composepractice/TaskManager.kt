package com.example.composepractice

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.composepractice.ui.theme.ComposePracticeTheme
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


@Composable
fun TaskManagerWithImage() {
    val image = painterResource(id = R.drawable.ic_task_completed)
    Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
        Image(
            painter = image,
            contentDescription = null,
            alignment = Alignment.Center
        )
        Text(
            text = "All tasks completed",
            fontSize = 24.sp,
            modifier = Modifier.padding(top =24.dp, bottom = 8.dp),
            textAlign = TextAlign.Center
        )
        Text(
            text = "Nice work!",
            textAlign = TextAlign.Center,
        )
    }
}

@Preview(showBackground = true)
@Composable
fun TaskManagerPreview() {
    ComposePracticeTheme {
        // A surface container using the 'background' color from the theme
        TaskManagerWithImage()
    }

}